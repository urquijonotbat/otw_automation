#!/bin/bash

# Detalles del Servidor
server_ip="bandit.labs.overthewire.org"
username="bandit2"
password="rRGizSaX8Mk1RTb1CNQoXTcYZWU6lgzi"
port="2220"

# Conectar por SSH al server con usuario, contraseña, direccion ip y puerto
sshpass -p "$password" ssh "$username"@"$server_ip" -p $port << EOF
    echo "La clave es:"
    cat spaces\ in\ this\ filename
    exit
EOF

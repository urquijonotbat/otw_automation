#!/bin/bash

# Detalles del Servidor
server_ip="bandit.labs.overthewire.org"
username="bandit3"
password="aBZ0W5EmUfAf7kHTQeOwd8bauFJ2lAiG"
port="2220"

# Conectar por SSH al server con usuario, contraseña, direccion ip y puerto
sshpass -p "$password" ssh "$username"@"$server_ip" -p $port << EOF
    echo "La clave es:"
    cd inhere
    cat .hidden
    exit
EOF
#!/bin/bash

# Detalles del Servidor
server_ip="bandit.labs.overthewire.org"
username="bandit6"
password="P4L4vucdmLnm8I7Vl7jG1ApGSfjYKqJU"
port="2220"

# Conectar por SSH al server con usuario, contraseña, direccion ip y puerto
sshpass -p "$password" ssh "$username"@"$server_ip" -p $port << EOF
    echo "La clave es:"
    cd /var/lib/dpkg/info/
    cat bandit7.password
    exit
EOF
#!/bin/bash

# Detalles del Servidor
server_ip="bandit.labs.overthewire.org"
username="bandit0"
password="bandit0"
port="2220"

# Conectar por SSH al server con usuario, contraseña, direccion ip y puerto
sshpass -p "$password" ssh "$username"@"$server_ip" -p $port << EOF
    echo "La clave es:"
    cat readme
    exit
EOF